// http://graphql.org/graphql-js/
var { graphql, buildSchema } = require('graphql')

// GraphQLスキーマ言語を使用してスキーマを構築する
var schema = buildSchema(`
    type Query {
        hello: String
        bye: String
        yajyu: Int
    }
`)
// rootは各APIエンドポイントのresolver関数を提供します
var root = {
    hello: () => {
        return 'Hello, World!'
    },
    bye: () => {
        return 'Good Bye!'
    },
    yajyu: () => {
        return 114514
    },
}
// GraphQLクエリ '{hello}'を実行してレスポンスを出力します
graphql(schema, '{ yajyu }', root).then((response) => {
    console.log(response)
})
